import ROOT

def mannWU(h1, h2):
  n1 = h1.GetSize()
  n2 = h2.GetSize()
  I1 = h1.Integral() + h1.GetBinContent(0) + h1.GetBinContent(n1-1) #adding the underflow and overflow bins by hand
  I2 = h2.Integral() + h2.GetBinContent(0) + h2.GetBinContent(n2-1)
  print I1, I2
  U = -0.5*I1*(I1 + 1)
  irank = 0
  for i in range(n1):
    rank = irank
    for j in range(n2):
      if (h1.GetBinCenter(i) > h2.GetBinCenter(j)): rank = rank + h2.GetBinContent(j)
      else:
        rank = rank + 0.5*h2.GetBinContent(j)*int(h1.GetBinCenter(i) == h2.GetBinCenter(j))
        break
    rank = rank + 0.5*(h1.GetBinContent(i) + 1)
    U = U + rank*h1.GetBinContent(i)
    irank =  irank + h1.GetBinContent(i)
  print U
  return U/(I1*I2)

def main():
  #f1 = ROOT.TFile.Open("Events.root", 'READ')
  #h1 = f1.Get('events')
  #f2 = ROOT.TFile.Open("Eventsshifted.root", 'READ')
  #h2 = f2.Get('events')
  #f3 = ROOT.TFile.Open("Eventsoverlaped.root", 'READ')
  #h3 = f3.Get('events')
  #print mannWU(h1, h1)
  #print mannWU(h2, h2)
  #print mannWU(h2, h1)
  #print mannWU(h2, h2)
  #print mannWU(h1, h3)
  #print mannWU(h3, h1)
  #print mannWU(h3, h2)
  #print mannWU(h2, h3)
  fA = ROOT.TFile.Open("A.root", 'READ')
  hA = fA.Get('events')
  fB = ROOT.TFile.Open("B.root", 'READ')
  hB = fB.Get('events')
  print mannWU(hA, hB)
  #print mannWU(hB, hA)

if __name__ == '__main__':
  main()

