"""
NN-optimization-4bit/utils/evalHist.py

A wrapper for running the Mann-Whitney U test. Takes in a root file and
histogram names, and prints the z value.

python evalHist.py --infile $wdir/p100.root -h1 ROC_1vs2_all_allp --h2 \
    ROC_1vs2_all_alln

Note that higher z values indicate more separate histograms which indicates a
better distinction from the neural networks
"""



import argparse
import MannWhitneyUtest
import MannWhitneyUtestOld
import ROOT

def main():
  parse = argparse.ArgumentParser()
  parse.add_argument('--infile', required=True)
  parse.add_argument('--h1', required=True)
  parse.add_argument('--h2', required=True)
  parse.add_argument('--old', action='store_true')
  args = parse.parse_args()

  tfile = ROOT.TFile(args.infile, "READ")
  h1 = tfile.Get(args.h1)
  h2 = tfile.Get(args.h2)
  print h1.GetSize()

  if args.old:
    z = MannWhitneyUtestOld.mannWU(h1,h2)
  else:
    z = MannWhitneyUtest.mannWU(h1,h2)
  print("z =" + str(z))


if __name__ == '__main__':
  main()
