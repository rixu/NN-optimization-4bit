/* cutTo4bit.C - Riley Xu 6/21/17
 * Given an input file with tree "NNinput", reduces the entries in the branches
 * "NN_matrix0" to "NN_matrix48" from 8 bit to 4 bit resolution. The data
 * entries are scaled based on a max threshold, x. For values y lower than x, the
 * new value is floor(y/x * 16). Values equal and above x are set to 15. The
 * number of entries copied is specified by n. If n=0, then all entries are
 * copied. Writes the new tree to the specified output. A default name _n-cutx is
 * given if not specified.
 *
 * root -l -b -q "cutTo4bit.C(\"$pToT5\",\"out.root\",100000,100)"
 */

#include <string>
#include <TFile.h>
#include <TTree.h>

void cutTo4bit(string file, string outname, int n, double x) {
    TFile *f = new TFile(file.c_str());
    TTree *T = (TTree *)f->Get("NNinput");

    int nentries = T->GetEntries();
    if (n <= 0 || n > nentries) n = nentries;

    // SetBranchAddress's for the NN_matrices
    double data[49] = {0};
    for (int i=0; i<49; i++) {
        string branchname = "NN_matrix" + std::to_string(i);
        T->SetBranchAddress(branchname.c_str(), &data[i]);
    }

    // Make output file and tree
    if (outname.length() == 0) {
        file.resize(file.size()-5); // get file header
        outname = file;
        if (n < nentries) {
            outname += "_";
            outname += TString::Format("%d",n);
        }
        outname += "-cut";
        outname += TString::Format("%.0f",x);
        outname += ".root";
    }
    cout << "Writing to file: " << outname << endl;
    TFile *out = new TFile(outname.c_str(),"recreate");
    TTree *nT  = T->CloneTree(0); // copy branch structure, but 0 entries

    // Fill new tree
    for (int j=0; j<n; j++) {
        T->GetEntry(j);
        for (int i=0; i<49; i++) {
            data[i] = (data[i] < x) ? floor(data[i]*16/x) : 15;
        }
        nT->Fill();
    }

    nT->Print();
    nT->Write();
    delete f;
    delete out;
}



