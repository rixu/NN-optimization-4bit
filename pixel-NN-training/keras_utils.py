import resource
import sys
import time
import warnings
import string
import warnings
import gc
import time

from keras import activations
from keras import backend as K
from keras.callbacks import Callback, EarlyStopping
from keras.engine.topology import Layer
import numpy as np


def sizeof_fmt(num, suffix='B'):
    '''https://stackoverflow.com/questions/1094841/reusable-library-to-get-human-readable-version-of-file-size'''
    for unit in ['','k','M','G','T','P','E','Z']:
        if abs(num) < 1024.0:
            return "%3.1f%s%s" % (num, unit, suffix)
        num /= 1024.0
    return "%.1f%s%s" % (num, 'Y', suffix)


class Sigmoid(Layer):
    def __init__(self, alpha, **kwargs):
        super(Sigmoid, self).__init__(**kwargs)
        self.alpha = alpha

    def call(self, x, mask=None):
        return activations.sigmoid(self.alpha*x)

    def get_config(self):
        config = {'name': self.__class__.__name__,
                  'alpha': self.alpha}
        base_config = super(Sigmoid, self).get_config()
        return dict(list(base_config.items()) + list(config.items()))


class Profile(Callback):
    '''
    This class extends the keras Callback class. It saves timing and cpu info
    to an out file for every epoch.
    '''
    def __init__(self, file=None, epoch=0):
        if file is None:
            #self.outfile = sys.stdin
            self.outfile = None
        else:
            self.outfile = file

        self.epoch = epoch
        self.time_total = 0
        header = 'epoch val_loss time_epoch time_total time_cpu maxrss\n'
        with open(self.outfile, 'a') as f:
           f.write(header)

    def on_epoch_begin(self, epoch, logs):
        self.t0 = time.time()

    def on_epoch_end(self, epoch, logs):
        time_epoch = time.time() - self.t0
        self.time_total += time_epoch
        usage = resource.getrusage(resource.RUSAGE_SELF)

        epo = ('%d' %self.epoch).ljust(4)
        vloss = '%9.6f' %logs.get('val_loss')
        t_epo = ('%d' %time_epoch).rjust(4)
        t_tot = ('%d' %(self.time_total/60)).rjust(4)
        t_cpu = ('%d' %((usage.ru_utime+usage.ru_stime)/60)).rjust(4)
        t_rss = sizeof_fmt(usage.ru_maxrss*1024.0).rjust(8)
        line = string.join((epo,vloss,t_epo,t_tot,t_cpu,t_rss)) + '\n'
        self.epoch = self.epoch + 1

        sys.stdout.write(line + '\n')
        sys.stdout.flush()
        with open(self.outfile, 'a') as f:
            f.write(line)

class MomentumScheduler(Callback):
    """Momentum scheduler.

    # Arguments
        schedule: a function that takes an epoch index as input
            (integer, indexed from 0) and returns a new
            momentum as output (float).
    """

    def __init__(self, schedule):
        super(MomentumScheduler, self).__init__()
        self.schedule = schedule

    def on_epoch_begin(self, epoch, logs=None):
        if not hasattr(self.model.optimizer, 'momentum'):
            raise ValueError('Optimizer must have a "momentum" attribute.')
        momentum = self.schedule(epoch)
        if not isinstance(momentum, (float, np.float32, np.float64)):
            raise ValueError('The output of the "schedule" function '
                             'should be float.')
        K.set_value(self.model.optimizer.momentum, momentum)
        #print(self.model.optimizer.momentum.get_value())



class Checkpoint(Callback):
    """Save the model after every epoch. Implements early stopping as well.

    Adapted from keras.callbacks.ModelCheckpoint and
    keras.callbacks.EarlyStopping

    # Arguments
        filepath: string, path to save the model file.
        monitor: quantity to monitor.
        verbose: verbosity mode, 0 or 1.
        save_best_only: if `save_best_only=True`,
            the latest best model according to
            the quantity monitored will not be overwritten.
        mode: one of {auto, min, max}.
            If `save_best_only=True`, the decision
            to overwrite the current save file is made
            based on either the maximization or the
            minimization of the monitored quantity. For `val_acc`,
            this should be `max`, for `val_loss` this should
            be `min`, etc. In `auto` mode, the direction is
            automatically inferred from the name of the monitored quantity.
        save_weights_only: if True, then only the model's weights will be
            saved (`model.save_weights(filepath)`), else the full model
            is saved (`model.save(filepath)`).
        period: Interval (number of epochs) between checkpoints.
        from_file: if checkpoint metadata should be loaded from a file
        config_file: if from_file, the source of the metadata
    """

    def __init__(self, filepath, monitor='val_loss', verbose=0,
                 save_best_only=False, save_weights_only=False,
                 mode='auto', period=1, from_file=False, config_file=None,
                 patience=5):
        super(Checkpoint, self).__init__()

        self.monitor = monitor
        self.verbose = verbose
        self.filepath = filepath
        self.save_best_only = save_best_only
        self.save_weights_only = save_weights_only
        self.period = period
        self.epochs_since_last_save = 0
        self.curr_epoch = 0
        self.config_file = config_file
        self.wait = 0
        self.stopped_epoch = 0
        self.patience = patience
        self.start_time = time.time()

        if mode not in ['auto', 'min', 'max']:
            warnings.warn('ModelCheckpoint mode %s is unknown, '
                          'fallback to auto mode.' % (mode),
                          RuntimeWarning)
            mode = 'auto'

        if mode == 'min':
            self.monitor_op = np.less
            self.best = np.Inf
        elif mode == 'max':
            self.monitor_op = np.greater
            self.best = -np.Inf
        else:
            if 'acc' in self.monitor or self.monitor.startswith('fmeasure'):
                self.monitor_op = np.greater
                self.best = -np.Inf
            else:
                self.monitor_op = np.less
                self.best = np.Inf

        if from_file:
            status = {}
            with open(config_file, 'r') as f:
                for line in f:
                    (name, eq, val) = str.partition(line, "=")
                    if not eq == "=":
                        warnings.warn('Checkpoint initialization '
                                'could not parse line: %s' %line, RuntimeWarning)
                    status[name] = float(val)
            self.epochs_since_last_save = status['epochs_since_last_save']
            self.best = status['best']
            self.curr_epoch = status['curr_epoch']
            self.wait = status['wait']
            self.start_time = status['start_time']


    def on_epoch_end(self, epoch, logs=None):
        logs = logs or {}
        self.epochs_since_last_save += 1

        if self.epochs_since_last_save >= self.period:
            self.epochs_since_last_save = 0
            filepath = self.filepath.format(epoch=epoch, **logs)
            if self.save_best_only:
                current = logs.get(self.monitor)
                if current is None:
                    warnings.warn('Can save best model only with %s available, '
                                  'skipping.' % (self.monitor), RuntimeWarning)
                else:
                    if self.monitor_op(current, self.best):
                        if self.verbose > 0:
                            print('Epoch %05d: %s improved from %0.5f to %0.5f,'
                                  ' saving model to %s'
                                  % (self.curr_epoch, self.monitor, self.best,
                                     current, filepath))
                        self.best = current
                        self.wait = 0
                        if self.save_weights_only:
                            self.model.save_weights(filepath, overwrite=True)
                        else:
                            self.model.save(filepath, overwrite=True)
                    else:
                        if self.wait >= self.patience:
                            self.stopped_epoch = epoch
                            self.model.stop_training = True
                        self.wait += 1
                        if self.verbose > 0:
                            print('Epoch %05d: %s did not improve, waiting'
                                  ' %d/%d' %
                                  (self.curr_epoch, self.monitor,
                                      self.wait, self.patience))
            else:
                if self.verbose > 0:
                    print('Epoch %05d: saving model to %s' % (self.curr_epoch, filepath))
                if self.save_weights_only:
                    self.model.save_weights(filepath, overwrite=True)
                else:
                    self.model.save(filepath, overwrite=True)
        self.curr_epoch += 1


    def on_train_end(self, logs=None):
        stop_time = time.time()
        if self.verbose > 0:
            print('Total time: %.2f hours' %((stop_time - self.start_time)/3600.))
            if self.stopped_epoch > 0:
                print('Epoch %05d: early stopping' % (self.stopped_epoch))

        self.model.save(self.filepath + ".tmp", overwrite=True)
        with open(self.config_file, 'w') as f:
            f.write('epochs_since_last_save=' +
                    str(self.epochs_since_last_save) + '\n')
            f.write('best=' + str(self.best) + '\n')
            f.write('curr_epoch=' + str(self.curr_epoch) + '\n')
            f.write('wait=' + str(self.wait) + '\n')
            f.write('start_time=' + str(self.start_time) + '\n')




"""This portion of the file doesn't seem to be used"""

"""This portion of the file implements the variable patience-based
early stopping strategy previously used in the AGILEPack setup. See
e.g.
https://svnweb.cern.ch/cern/wsvn/atlas-rjansky/AGILEPack/trunk/cli-src/train_interface.cxx
around l.250.

The code for the class is an adaptation of the EarlyStopping keras callback
(https://github.com/fchollet/keras/blob/master/keras/callbacks.py#L303, commit 85e51a0)

"""

class ThresholdEarlyStopping(EarlyStopping):
    def __init__(self, monitor='val_loss', min_epochs=10,
                 threshold=0.995, increase=1.75, verbose=0, mode='auto'):

        super(ThresholdEarlyStopping, self).__init__(
            monitor=monitor,
            patience=min_epochs,
            verbose=verbose,
            mode=mode
        )

        self.threshold = threshold
        self.increase = increase

    def on_epoch_end(self, epoch, logs={}):
        if epoch < self.patience:
            current = logs.get(self.monitor)
            if current is None:
                warnings.warn('Early stopping requires %s available!' %
                              (self.monitor), RuntimeWarning)

            if self.monitor_op(current, self.best):
                if self.monitor_op(current, self.threshold*self.best):
                    self.patience = max(self.patience, epoch * self.increase)
                self.best = current

        else:
            if self.verbose > 0:
                print('Epoch %05d: early stopping' % (epoch))

            self.model.stop_training = True
