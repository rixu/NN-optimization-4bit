/* ROC.cxx
 *
 * A program that modifies (+ truth, + predicted) probabilites, output from a
 * SQL query, into triplets for graphing in ROC_graph.cxx:
 *
 * (+ threshold, false + rate, true + rate)
 *
 * where + threshold is the threshold probability, above which events are
 * assigned as positive. Triplets are printed to stdout, separated by spaces
 * and newlines.
 *
 * usage: ./ROC (# condition positives) (# condition negatives)
 */


#include <float.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char *argv[])
{
	signal(SIGPIPE, SIG_DFL); // default

	if (argc != 3) {
		fprintf(stderr,
			"usage: %s <n. positives> <n. negatives>\n",
		        argv[0]);
		return 1;
	}

	double FP = 0, TP = 0; // false positives, true positives
	double fprev = DBL_MIN;

	double P = strtod(argv[1], NULL); // # condition postives
	double N = strtod(argv[2], NULL); // # condition negatives

	size_t len = 2048;
	char *lineptr = (char*)malloc(sizeof(char) * len);

	printf("1 0 0\n");

	while (getline(&lineptr, &len, stdin) > -1) {
		double key; // 1 or 0 -> positive or negative
		double f;   // predicted positive probability
		if (sscanf(lineptr, "%lf %lf\n", &key, &f) != 2) {
			fprintf(stderr, "ERROR: %s: malformed line: %s\n", argv[0], lineptr);
			return 1;
		}

		if (f != fprev) {
			printf("%lf %lf %lf\n", f, FP / N, TP / P);
			fprev = f;
		}
        // FP/N = false +/condition - = false + rate
        // TP/P = true  +/condition + = true  + rate

		if (key == 1.0)
			++TP;
		else
			++FP;
	}

	printf("0 1 1\n");

    free(lineptr); // added 6/21/17

	return 0;
}
