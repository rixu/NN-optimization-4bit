"""
keras_schedules.py: Riley Xu - 7/5/17

Schedules for the learning rate and momentum callbacks
"""

def lr_wrapper(lr_schedule, learning_rate, curr_epoch):
    '''learning rate scheduler for callback'''
    def lr_scheduler(epoch):

        if lr_schedule == "thirties":
            if curr_epoch + epoch > 60:
                return learning_rate/5
            elif curr_epoch + epoch > 30:
                return learning_rate/3
            else:
                return learning_rate

        elif lr_schedule == "drop100":
            if curr_epoch + epoch >= 100:
                if curr_epoch + epoch == 100:
                    print("Changing learning rates from %f to %f",learning_rate,learning_rate/2)
                return learning_rate/2
            else:
                return learning_rate

        else:
            print("Warning: schedule <" + lr_schedule + "> not matched")
            return learning_rate

    return lr_scheduler


def momentum_wrapper(momentum_schedule, momentum, curr_epoch):
    '''learning rate scheduler for callback'''
    def momentum_scheduler(epoch):

        if momentum_schedule == "drop20":
            if curr_epoch + epoch > 20:
                return momentum/2
            else:
                return momentum

        else:
            print("Warning: schedule <" + momentum_schedule + "> not matched")
            return momentum

    return momentum_scheduler



