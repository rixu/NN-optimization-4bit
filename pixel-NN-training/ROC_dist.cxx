/* ROC_dist.cxx
 *
 * A program that takes (+ truth, + predicted) probabilites, output from a
 * SQL query, and creates two histograms of the predicted + probabilities for
 * true + and true - events. Images of the hisograms are created as well
 *
 * The output name template should be specified, as well as the directory the
 * ROOT file containing the histograms should be created in (test-driver adds
 * all histograms to a single file from within a temp directory). The files are
 * named
 *      outname.png
 *      outdir/outname-dist.root
 *          "<TH1F>outnamep"
 *          "<TH1F>outnamen"
 *
 * usage: ./ROC_dist outname tempdir
 */


#include <float.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <TFile.h>
#include <TH1F.h>
#include <THStack.h>
#include <TCanvas.h>
#include <TImage.h>

int main(int argc, char *argv[])
{
	signal(SIGPIPE, SIG_DFL); // default

    if (argc != 3) {
		fprintf(stderr, "usage: %s outname temp_directory \n", argv[0]);
		return 1;
	}

    std::string outname(argv[1]);
    std::string outdir(argv[2]);
    std::string hPosName = outname + "p";
    std::string hNegName = outname + "n";
    std::string outfile  = outdir + "/" + outname + "-dist.root";
    std::string outimg   = outname + ".png";

    THStack *hStack = new THStack(argv[1], "Predicted probabilities for positive rating");
    TH1F *hPos = new TH1F(hPosName.c_str(),"Predicted probabilities for true positives",100,0,1);
    TH1F *hNeg = new TH1F(hNegName.c_str(),"Predicted probabilities for true negatives",100,0,1);
    hPos->SetFillColor(kRed);
    hNeg->SetFillColor(kBlue);

	size_t len = 2048;
	char *lineptr = (char*)malloc(sizeof(char) * len);

	while (getline(&lineptr, &len, stdin) > -1) {
		double key; // 1 or 0 -> positive or negative
		double p;   // predicted positive probability

		if (sscanf(lineptr, "%lf %lf\n", &key, &p) != 2) {
			fprintf(stderr, "ERROR: %s: malformed line: %s\n", argv[0], lineptr);
			return 1;
		}

		if (key == 1.0)
            hPos->Fill(p);
		else
            hNeg->Fill(p);
	}

    hStack->Add(hPos);
    hStack->Add(hNeg);

    TFile *out = new TFile(outfile.c_str(), "recreate");
    hPos->Write();
    hNeg->Write();

    TCanvas *c1 = new TCanvas;
    hStack->Draw("nostackb");
    TImage *img = TImage::Create();
    img->FromPad(c1);
    img->WriteImage(outimg.c_str());

    free(lineptr);
    out->Close();
    delete hStack;
    delete hPos;
    delete hNeg;
    delete c1;
    delete img;

	return 0;
}
