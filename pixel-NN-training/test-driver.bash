#!/bin/bash
#
# ./test-driver.bash type{number, pos1, error1x} input.db output [SIZEY]
#
# A general test script for analyzing the performance of a neural network. The
# neural network should be created and trained with trainNN_keras.py, and the
# output matched with truth values using evalNN_keras.py. The script will then
# analyze the results and create ROOT graphs in the output file.


export TYPE=$1
export DB=$2
export OUTPUT=$3
export SIZEY=${4:-7}

NJOBS=${NJOBS:-10}

export SCRIPT=$(readlink -f $0) # Absolute path to this script.
export SCRIPTPATH=`dirname $SCRIPT` # Absolute path this script is in.
export HOME='/afs/cern.ch/user/r/rixu'
export tmp=$(mktemp -d)

###############         FUNCTIONS           ###################
calc_ROC () {
    name=$1
    sql_count_p=$2
    sql_count_n=$3
    sql_ROC=$4
    tmpfile=$(mktemp)

    sqlite3 -separator " " $DB "$sql_ROC" > $tmpfile
	$SCRIPTPATH/ROC $(sqlite3 $DB "$sql_count_p") $(sqlite3 $DB "$sql_count_n") < $tmpfile \
	| $SCRIPTPATH/ROC_Graph $name $tmp/$name.root

    $SCRIPTPATH/ROC_dist $name $tmp < $tmpfile
    $SCRIPTPATH/ROC_MWU $DB-$name < $tmpfile > $tmp/$name-U.txt
}
export -f calc_ROC

calc_res () {
    name=$1
    sql=$2

    sqlite3 -separator " " $DB "$sql" \
	| $SCRIPTPATH/residuals $name $tmp/$name.root $SIZEY $(echo $TYPE | tr -d "pos")
}
export -f calc_res

calc_err () {
    name=$1
    sql=$2

    etype=$(echo $TYPE | tr -d "error")
    npart=${etype:0:1}
    direc=${etype:1:1}

    sqlite3 -separator " " $DB "$sql" \
	| $SCRIPTPATH/validate_error $name $tmp/$name.root $SIZEY $npart $direc
}
export -f calc_err


###############           MAIN             ###################
# gensql.py generates SELECT commands seperated by | and newlines. Each set of
# SELECT commands separated by \n is passed as arguments to a calc function, with
# the arguments separated by |. Each set is run in parallel via the parallel
# command.

if [ $TYPE = "number" ]
then
    python2 $SCRIPTPATH/gensql.py --type $TYPE --no_layers \
	| $HOME/bin/parallel -P$NJOBS --colsep '\|' calc_ROC
    cat $tmp/*.txt | sort -k 1,1 > $DB-number.txt
elif [ $TYPE = "pos1" ] || [ $TYPE = "pos2" ] || [ $TYPE = "pos3" ]
then
    python2 $SCRIPTPATH/gensql.py --type $TYPE --sizeY $SIZEY \
	| $HOME/bin/parallel -P$NJOBS --colsep '\|' calc_res
else
    python2 $SCRIPTPATH/gensql.py --type $TYPE --sizeY $SIZEY \
	| $HOME/bin/parallel -P$NJOBS --colsep '\|' calc_err
fi


hadd -f $OUTPUT $tmp/*.root

#rm -r $tmp
