"""
plotLosses.py: Riley Xu 7/3/17
Creates plots of validation and training loss from given input files. Plots
against epoch numbers, assumed to start and increment with 1.

python plotLosses.py --training-loss tloss.txt --validation-loss vloss.txt
          [--out outname.png]
"""

import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import argparse

def plot_loss(training_loss, validation_loss, outname):
  t_losses = []
  v_losses = []
  t_file = open(training_loss)
  v_file = open(validation_loss)

  for line in t_file:
    t_losses.append(float(line))

  for line in v_file:
    v_losses.append(float(line))

  x = range(1,len(t_losses)+1)

  t_line, = plt.plot(x,t_losses,'-b',label = 'Training Loss')
  v_line, = plt.plot(x,v_losses,'-r',label = 'Validation Loss')
  plt.xlabel('Epoch')
  plt.ylabel('Loss')
  plt.legend(handles=[t_line, v_line])
  plt.title('Training Loss: ' + training_loss + '\nand\nValidation Loss: ' + \
      validation_loss)
  plt.tight_layout()
  plt.savefig(outname)

  t_file.close()
  v_file.close()

def main():
  parse = argparse.ArgumentParser()
  parse.add_argument("-t","--training-loss", required=True)
  parse.add_argument("-v","--validation-loss", required=True)
  parse.add_argument("-o","--out", default="out.png")
  args = parse.parse_args()

  plot_loss(args.training_loss, args.validation_loss, args.out)

if __name__ == '__main__':
  main()
