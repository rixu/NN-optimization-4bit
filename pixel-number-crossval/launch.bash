#!/bin/bash
#
# Script for launching the training and eval programs for the pixel NNs. For
# the training, the script will reload the python program every 20 epochs, to
# alleviate the memory leak in the training script. There is currently a
# hard-coded 1000 epoch cap.
#
# This launch file only works for the number NN (the pos and error segments
# have been cut, although can easily be copied back in). It uses a
# cross-validation implementation.
#
# Example:
#
# source activate keras
# nohup bash $pdir/launch.bash -f $wdir/pToT5_10000-cut100.root -t $wdir/pToT1_1000-cut100.root
#       -o p100 -y number > p100.out.txt &
#
# Make sure to NOT setupATLAS yet


function usage()
{
    echo "usage: $0 <-option value>"
    echo "    -f, --train file              : training file path"
    echo "    -t, --test file               : testing file path"
    echo "    -o, --out name                : output header name"
    echo "    -y, --type type               : NN type {number,posN,errorNz}"
    echo ""
    echo "  ----    OPTIONAL    ----"
    echo ""
    echo "    -h, --help                    : print this message"
    echo "    -l, --learning-rate RATE      : learning rate (default = 0.08)"
    echo "    -m, --momentum MOMENTUM       : momentum (default = 0.4)"
    echo "    -r, --regularizer REGULARIZER : regularizer (default = 0.0000001)"
    echo "    -s, --lr-schedule NAME        : learning rate schedule name"
    echo "    -a, --momentum-schedule NAME  : momentum schedule name"
    echo "    -p, --patience PATIENCE       : patience (default = 20)"
    echo "    -V, --validation-num NUM      : number of cross-validations (default = 4)"
    echo "    --start-epoch EPOCH           : starting epoch for training (default = 0)"
    echo "    --max-epochs MAX_EPOCHS       : max number of epochs for training (default = 1000)"
    echo "    --numpy-seed SEED             : RNG seed for numpy"
}

unset NUMPY_SEED
unset PYTHONHASHSEED
################          Argument Parsing         #################
if [ $# -lt 8 ]; then usage; exit 1; fi
while [ -n "${1-}" ]; do
    case $1 in
        -f | --train )          shift
                                export TRAINING=$1
                                ;;
        -t | --test )           shift
                                export TEST=$1
                                ;;
        -o | --out )            shift
                                export NAME=$1
                                ;;
        -y | --type )           shift
                                export TYPE=$1
                                ;;
        -l | --learning-rate )  shift
                                export LEARNING_RATE=$1
                                ;;
        -m | --momentum )       shift
                                export MOMENTUM=$1
                                ;;
        -r | --regularizer )    shift
                                export REGULARIZER=$1
                                ;;
        -s | --lr-schedule )    shift
                                export LR_SCHEDULE=$1
                                ;;
        -a | --momentum-schedule )
                                shift
                                export MOMENTUM_SCHEDULE=$1
                                ;;
        -p | --patience )       shift
                                export PATIENCE=$1
                                ;;
        -V | --validation-num ) shift
                                export VAL_NUM=$1
                                ;;
        --start-epoch )         shift
                                export EPOCH=$1
                                ;;
        --max-epochs )          shift
                                export MAX_EPOCHS=$1
                                ;;
        --numpy-seed )          shift
                                export NUMPY_SEED=$1
                                export PYTHONHASHSEED=0
                                ;;
        --train-only )          TRAIN_ONLY=1
                                ;;
        -h | --help )           usage
                                exit
                                ;;
        * )                     usage
                                exit 1
    esac
    shift
done

if [ -z ${LEARNING_RATE+x} ]; then
    export LEARNING_RATE=0.08
fi
if [ -z ${MOMENTUM+x} ]; then
    export MOMENTUM=0.4
fi
if [ -z ${REGULARIZER+x} ]; then
    export REGULARIZER=0.0000001
fi
if [ -z ${LR_SCHEDULE+x} ]; then
    export LR_SCHEDULE=0
fi
if [ -z ${MOMENTUM_SCHEDULE+x} ]; then
    export MOMENTUM_SCHEDULE=0
fi
if [ -z ${PATIENCE+x} ]; then
    export PATIENCE=20
fi
if [ -z ${VAL_NUM+x} ]; then
    export VAL_NUM=4
fi
if [ -z ${EPOCH+x} ]; then
    export EPOCH=0
fi
if [ -z ${MAX_EPOCHS+x} ]; then
    export MAX_EPOCHS=1000
fi
if [ -z ${TRAIN_ONLY+x} ]; then
    TRAIN_ONLY=0
fi

export TYPE=number
export SIZEX=7
export SIZEY=7

export pixelDir='/afs/cern.ch/user/r/rixu/private/NN-optimization-4bit/pixel-number-crossval'
export HOME='/afs/cern.ch/user/r/rixu'
export currDir=$(pwd)
#################         FUNCTIONS        #################

function train()
{
    # set exit status of a pipe to last non-zero value
    set -o pipefail
    local epoch
    epoch=$EPOCH

    while [ $epoch -lt $MAX_EPOCHS ]; do
        python $pixelDir/trainNN_keras.py \
            --training-input $TRAINING \
            --output $NAME \
            --config <(python $pixelDir/genconfig.py --type $TYPE --sizeX $SIZEX --sizeY $SIZEY) \
            --validation_i $1 \
            --validation_num $VAL_NUM \
            --structure 25 20 \
            --output-activation softmax \
            --l2 $REGULARIZER \
            --learning-rate $LEARNING_RATE \
            --momentum $MOMENTUM \
            --batch 60 \
            --max-epochs 20 \
            --patience $PATIENCE \
            --verbose \
            --profile \
            --weights "$currDir/${NAME}_cv$1.weights.hdf5" \
            --current-epoch $epoch \
            --lr-schedule $LR_SCHEDULE \
            --momentum-schedule $MOMENTUM_SCHEDULE \
            >> ${NAME}_cv$1.out.txt
        if [ $? -eq 1 ]; then break; fi
        epoch=$((epoch + 20))
        echo "Finished $epoch/$MAX_EPOCHS epochs in training $1"
    done
}
export -f train


#################           MAIN           #################
source /afs/cern.ch/user/r/rixu/anaconda2/bin/activate keras

# treat unset variables as error
set -u
# exit on failed pipe
#set -e


echo "       BEGINNING TRAINING        " >&2
seq 0 $((VAL_NUM-1)) | $HOME/bin/parallel train
if [ $TRAIN_ONLY -eq 1 ]; then exit 0; fi


echo "        BEGINNING EVALUATION       " >&2

python $pixelDir/evalNN_keras.py \
    --input $TEST \
    --name $NAME \
    --validation-num $VAL_NUM \
    --config <(python $pixelDir/genconfig.py --type $TYPE --sizeX $SIZEX --sizeY $SIZEY) \


echo "        BEGINNING TESTING       " >&2
set +e
set +u

. /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/user/atlasLocalSetup.sh
. /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/packageSetups/localSetup.sh root
$pixelDir/test-driver.bash $TYPE $NAME.db $NAME.root $SIZEY
