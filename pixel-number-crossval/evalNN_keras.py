""" eval_nn: module to evaluate a dataset using a neural network """

import argparse
import itertools as it
import sqlite3

import keras.models
import numpy as np

import keras_utils
import utils
import root_utils

__all__ = ['eval_nn']


def eval_nn(inputp,
            name,
            validation_num,
            config,
            output=None): \
            # pylint: disable=too-many-arguments
    """ evaluate a dataset  with a neural network stored on disk

    arguments:
    inputp -- path to the ROOT dataset
    name -- path to the keras model file header name
    validation_num -- the number of cross-validation folds
    config -- path to the branches config file
    output -- output name for the sqlite database (overwrites the 'test' table)

    Creates a database file in output (test.db) with columns meta,
    output_truth, output_pred.
    """

    models = []
    if output is None:
        output = name + ".db"
    normalization = name + "_cv0.normalization.txt"

    for i in range(validation_num):
        model_name = name + "_cv" + str(i)
        model = model_name + ".model.yaml"
        weights = model_name + ".weights.hdf5"

        model = keras.models.model_from_yaml(
            open(model, 'r').read(),
            custom_objects={'Sigmoid': keras_utils.Sigmoid}
        )
        model.load_weights(weights)
        models.append(model)

    _eval_dataset(
        models=models,
        path=inputp,
        tree='NNinput',
        branches=utils.get_data_config_names(config, meta=True),
        norm = utils.load_normalization(normalization),
        dbpath=output,
    )


def _eval_dataset(models,
                  path,
                  tree,
                  branches,
                  norm,
                  dbpath,
                  batch=128): \
                  # pylint: disable=too-many-arguments

    # branches is a 2d array of names filled from the config file
    #   branches[0] = inputs  : NN_matrix#, etc.
    #   branches[1] = targets : NN_nparticles#, for numberNN
    #   branches[2] = metadata


    dbconn = sqlite3.connect(dbpath)
    _prepare_db(
        dbconn=dbconn,
        meta_branches=branches[2],
        y_branches=branches[1]
    )

    data_generator = root_utils.generator(
        path,
        tree=tree,
        branches=branches[:2],
        batch=batch,
        normalization=norm,
        loop=False
    )
    # generator of (normalized data, targets) in batch sizes

    meta_generator = root_utils.generator(
        path,
        tree=tree,
        branches=(branches[2], []),
        batch=batch,
        normalization=None,
        loop=False
    )
    # generator of (meta data, None) in batch sizes

    for (xbatch, ybatch), (meta, _) in it.izip(data_generator, meta_generator):
        y_preds = []
        for model in models:
            y_preds.append(model.predict(xbatch, batch_size=xbatch.shape[0]))

        _insert_into_db(
            dbconn,
            meta,
            y_truth=ybatch,
            y_pred=np.mean(y_preds,0)
        )

    dbconn.commit()


def _insert_into_db(dbconn, meta, y_truth, y_pred):

    mcol = meta.shape[1]
    ycol = y_truth.shape[1]

    shape = (meta.shape[0], mcol + 2*ycol)

    meta_begin = 0
    meta_end = meta_begin + mcol
    truth_begin = meta_end
    truth_end = truth_begin + ycol
    pred_begin = truth_end
    pred_end = pred_begin + ycol

    data = np.empty(shape)
    data[:, meta_begin:meta_end] = meta
    data[:, truth_begin:truth_end] = y_truth
    data[:, pred_begin:pred_end] = y_pred

    sql = 'INSERT INTO test VALUES ('
    sql += ','.join(['?'] * shape[1])
    sql += ')'

    dbconn.executemany(sql, data)


def _prepare_db(dbconn, meta_branches, y_branches):

    dbconn.execute('DROP TABLE IF EXISTS test')
    sql = 'CREATE TABLE test ('
    sql += ','.join([br + ' REAL' for br in meta_branches])
    sql += ','
    sql += ','.join([br + '_TRUTH REAL' for br in y_branches])
    sql += ','
    sql += ','.join([br + '_PRED REAL' for br in y_branches])
    sql += ')'
    dbconn.execute(sql)
    dbconn.commit()


def _main():
    parse = argparse.ArgumentParser()
    parse.add_argument("--input", required=True)
    parse.add_argument("--name", required=True)
    parse.add_argument("--validation-num", required=True, type=int)
    parse.add_argument("--config", required=True)
    parse.add_argument("--output", default=None)
    args = parse.parse_args()

    eval_nn(
        args.input,
        args.name,
        args.validation_num,
        args.config,
        args.output
    )


if __name__ == '__main__':
    _main()
